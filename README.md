# practicas_2019
2019-07-01
Codigo python para las charlas dictadas en el IGEPN 2019-06

## Troubleshooting

Mostrar la ventana de plot en ubuntu 

Modificar el archivo de configuración de matplotlib
```bash
#File to edit:
/usr/local/lib/python2.7/dist-packages/matplotlib/mpl-data/matplotlibrc
~/.config/matplotlib/matplotlibrc
#cambiar la siguiente línea o comentar (depende del mensage de error al plotear)
backend : TkAgg 
```
Instalar el software python-tk
```bash
#UBUNTU
apt install python-tk

#CENTOS. 
#Instalar tkinter para python
yum install python27-tkinter.x86_64
```

