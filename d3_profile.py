
              
for station in station_list:
    
    print("Get data and plot for:%s" %station['cod'])
    data=d1_obspy_function.get_data_v2(station['net'], station['cod'], station['loc'], station['cha'], diaUTC, diaUTC+data_window)
    
    if data !=None and len(data)!=0 :
        print(data)
        data.plot(outfile="./data/%s.png" %station['cod'])
    else:
        print("No data found for:%s" %station['cod'])


"""CALL OBSPY FUNCTION N TIMES:
MOUNT ARCHIVE FIRST!!! 
ARCHIVE: BEST PERFORMANCE: WACERO  

diaUTC=UTCDateTime("2019-06-12 00:00:00")
data_window=86400

station_list=[{"net":"EC","cod":"BREF","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BVC2","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BNAS","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BMOR","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BTAM","loc":"","cha":"BHZ"}
              ]
              
for station in station_list:
    
    print("Get data and plot for:%s" %station['cod'])
    data=d1_obspy_function.get_data_v3(station['net'], station['cod'], station['loc'], station['cha'], diaUTC, diaUTC+data_window)
    
    if data !=None and len(data)!=0 :
        print(data)
        data.plot(outfile="./data/%s.png" %station['cod'])
    else:
        print("No data found for:%s" %station['cod'])
        
        
"""   
        
