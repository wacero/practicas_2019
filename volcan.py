


class volcan:
    """
    Ejemplo sencillo de una clase
    """
    
    
    def __init__(self,nombre,altura,latitud,longitud):
        
        self.nombre=nombre
        self.altura=altura
        self.latitud=latitud
        self.longitud=longitud
        
    def calcular_volumen(self,amplitud_waveform):
        """
        Funcion que retorna el volumen de ceniza segun la amplitud
        """
        CONSTANTE=2
        volumen=amplitud_waveform**3/CONSTANTE
        
        return volumen
    
    
    def calcular_volumen_case(self,amplitud_waveform,tipo):
        
        if tipo=="A":
            CONSTANTE=2
            return amplitud_waveform**3/CONSTANTE
        if tipo=="B":
            CONSTANTE=3
            return amplitud_waveform**3/CONSTANTE
        
        