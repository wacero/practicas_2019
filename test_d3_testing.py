
import numpy as np
import d3_testing


data=[1,2,3,4,5,6,7,8,9,10]

rms_calculated=((np.square(data)).sum()/len(data))**0.5

""" 
Test de una funcion sin errores
"""

def test_rsam_calculate_rsam_ok():
    'Test the calculate_rms method'
 
    assert d3_testing.calculate_rms(data)==rms_calculated


"""
Test de una funcion con un error
"""

def test_rsam_calculate_rsam_fail():
    'Test the calculate_rms method'
 
    assert d3_testing.calculate_rms_fail(data)==rms_calculated
    
