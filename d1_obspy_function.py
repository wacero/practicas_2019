"""


"""
from obspy.clients.arclink import Client as cliente_arclink 
from obspy.clients.filesystem.sds import Client as clientArchive

cliente_datos=cliente_arclink('usuario','192.168.131.2',18001)

cliente_datos_archive=clientArchive('/datosSC3')



def get_data(network,station,location,channel,starttime,endtime):
    
    data_stream=cliente_datos.get_waveforms(network, station, location, channel, starttime, endtime, route=False, compressed=False)
    
    return data_stream
    



def get_data_v2(network,station,location,channel,starttime,endtime):
    
    try:
        return cliente_datos.get_waveforms(network, station, location, channel, starttime, endtime, route=False, compressed=False)
    
    except Exception as e:
        print("Error in getting data stream for:%s. Error was:%s" %(station,str(e)))
    

def get_data_v3(network,station,location,channel,starttime,endtime):
    
    print(cliente_datos_archive)
    
    try:
        return cliente_datos_archive.get_waveforms(network, station, location, channel, starttime, endtime)
    
    except Exception as e:
        print("Error in getting data stream for:%s. Error was:%s" %(station,str(e)))
    

    
    