import d3_debug

"""
Esta funcion se ejecutar sin problemas
"""

#d3_debug.debug_ok()

"""
Esta funcion se ejecutara al inicio sin problemas y luego fallara
Utilizar PDB desde la terminal de python para hallar el problema
"""
d3_debug.debug_fail()


"""
Esta funcion tambien fallara pero imprime en pantalla el valor que genera el error

"""

#d3_debug.debug_print()

"""
Esta funcion utiliza logging como una forma de debug
"""

#d3_debug.debug_log()